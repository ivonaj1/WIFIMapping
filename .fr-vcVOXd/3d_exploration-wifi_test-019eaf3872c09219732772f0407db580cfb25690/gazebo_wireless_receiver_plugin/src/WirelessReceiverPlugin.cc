#include "WirelessReceiverPlugin.hh"
#include "gazebo/math/Rand.hh"
#include "gazebo/msgs/msgs.hh"
#include "gazebo/sensors/SensorFactory.hh"
#include "gazebo/sensors/SensorManager.hh"
#include "gazebo/sensors/WirelessReceiver.hh"
#include "gazebo/sensors/WirelessTransmitter.hh"
#include "gazebo/transport/Node.hh"
#include "gazebo/transport/Publisher.hh"
using namespace gazebo;
using namespace sensors;

GZ_REGISTER_SENSOR_PLUGIN(WirelessReceiverPlugin)

/////////////////////////////////////////////////
WirelessReceiverPlugin::WirelessReceiverPlugin() : SensorPlugin()
{
}

/////////////////////////////////////////////////
WirelessReceiverPlugin::~WirelessReceiverPlugin()
{
}

/////////////////////////////////////////////////
void WirelessReceiverPlugin::Load(sensors::SensorPtr _sensor, sdf::ElementPtr /*_sdf*/)
{
    // Get the parent sensor.
    this->parentSensor =
            boost::dynamic_pointer_cast<sensors::WirelessReceiver>(_sensor);

    // Make sure the parent sensor is valid.
    if (!this->parentSensor)
    {
        gzerr << "WirelessReceiverPlugin requires a WirelessReceiver.\n";
        return;
    }

    // Connect to the sensor update event.
    this->updateConnection = this->parentSensor->ConnectUpdated(
            std::bind(&WirelessReceiverPlugin::OnUpdate, this));

    // Make sure the parent sensor is active.
    this->parentSensor->SetActive(true);

    std::cout << "wireless receiver active";


}


/////////////////////////////////////////////////
void WirelessReceiverPlugin::OnUpdate()
{
    std::string txEssid;
    sensors::SensorPtr _sensor;
    double rxPower;
    bool k;
    math::Pose pose_rx;
    pose_rx=this->parentSensor->GetPose();
    msgs::WirelessNodes wirelessinfo;

   // k=this-parentSensor->UpdateImpl(true);
    gazebo::sensors::WirelessTransmitter  wirelessTransmitter ;
    //sensors::WirelessTransmitterPtr  wirelessTransmitter ;
   // wirelessTransmitter=boost::dynamic_pointer_cast<sensors::WirelessTransmitter>(_sensor);
   //std::cout << pose_rx << " \n";

   std::cout << this->parentSensor->GetGain()<< " \n";

    std::string world_name=this->parentSensor->GetWorldName();


   wirelessTransmitter.Init();

    wirelessTransmitter.Load(world_name);
    std::cout << wirelessTransmitter.GetGain()<< " \n";

    rxPower = wirelessTransmitter.GetSignalStrength(pose_rx,this->parentSensor->GetGain());

    std::cout << rxPower << " \n";

}

